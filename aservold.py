import asyncio
import os  # Импортируем модуль os для выхода из программы

async def handle_echo(reader, writer):
    """
    Асинхронная функция-обработчик для входящих соединений.
    Читает данные от клиента, отправляет их обратно, и завершает соединение
    при получении сообщения 'exit' или пустой строки.
    """
    while True:
        data = await reader.read(1024)  # Читаем максимум 1024 байта данных от клиента
        message = data.decode()  # Декодируем байтовые данные в строку
        addr = writer.get_extra_info('peername')  # Получаем адрес клиента
        print(f"Received {message!r} from {addr!r}")  # Выводим полученное сообщение и адрес клиента

        if message.strip() == 'exit' or message.strip() == '':
            # Если получена команда 'exit' или пустая строка, выходим из цикла
            break

        writer.write(data)  # Отправляем полученные данные обратно клиенту
        await writer.drain()  # Ожидаем, пока буфер отправки опустеет
        print(f"Sent {message!r} back to {addr!r}")  # Выводим, что данные были отправлены обратно

    writer.close()  # Закрываем соединение с клиентом

    if message.strip() == 'exit':
        # Если получена команда 'exit', завершаем программу
        os._exit(0)

async def main():
    """
    Главная функция, создающая и запускающая сервер.
    """
    server = await asyncio.start_server(
        handle_echo, '127.0.0.1', 8888)
    # Создаем сервер, привязываем его к локальному хосту на порту 8888
    # и регистрируем обработчик handle_echo для входящих соединений

    addr = server.sockets[0].getsockname()  # Получаем адрес сервера
    print(f"Serving on {addr}")  # Выводим адрес сервера

    async with server:
        # Запускаем сервер в контекстном менеджере
        await server.serve_forever()  # Ожидаем бесконечно входящие соединения

# Запускаем главную функцию в асинхронной петле
asyncio.run(main())
