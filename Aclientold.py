import asyncio  # Импортируем модуль asyncio для асинхронного программирования

async def tcp_echo_client(message):
    """
    Асинхронная функция, которая подключается к TCP-серверу,
    отправляет сообщение и печатает ответ сервера.
    """
    reader, writer = await asyncio.open_connection('127.0.0.1', 8888)
    # Устанавливаем соединение с сервером на локальном хосте (127.0.0.1) по порту 8888
    # reader - объект для чтения данных из сокета
    # writer - объект для записи данных в сокет

    print(f'Send: {message!r}')  # Выводим отправляемое сообщение
    writer.write(message.encode())  # Кодируем сообщение в байты и отправляем его

    data = await reader.read(100)  # Читаем максимум 100 байт данных из сокета
    print(f'Received: {data.decode()!r}')  # Декодируем полученные байты и выводим

    writer.close()  # Закрываем соединение
    await writer.wait_closed()  # Ожидаем, пока соединение будет полностью закрыто

# Запускаем асинхронную функцию tcp_echo_client с аргументом 'Hello, World!'
asyncio.run(tcp_echo_client('Hello, World!'))

# Запускаем асинхронную функцию tcp_echo_client с аргументом 'exit'
asyncio.run(tcp_echo_client('exit'))